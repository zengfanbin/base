#ifdef WIN32

#include "charset.h"
#include <Windows.h>
#include <vector>

std::wstring UT2WC(const char* buf)
{
	int len = MultiByteToWideChar(CP_UTF8, 0, buf, -1, NULL, 0);
    std::vector<wchar_t> unicode(len);
	MultiByteToWideChar(CP_UTF8, 0, buf, -1, &unicode[0], len);

    return std::wstring(&unicode[0]);
}

std::string WC2UT(const wchar_t* buf)
{
	int len = WideCharToMultiByte(CP_UTF8, 0, buf, -1, NULL, 0, NULL, NULL);
    std::vector<char> utf8(len);
	WideCharToMultiByte(CP_UTF8, 0, buf, -1, &utf8[0], len, NULL, NULL);

    return std::string(&utf8[0]);
}

std::wstring MB2WC(const char* buf)
{
	int len = MultiByteToWideChar(CP_ACP, 0, buf, -1, NULL, 0);
    std::vector<wchar_t> unicode(len);
	MultiByteToWideChar(CP_ACP, 0, buf, -1, &unicode[0], len);

    return std::wstring(&unicode[0]);
}

std::string WC2MB(const wchar_t* buf)
{
	int len = WideCharToMultiByte(CP_ACP, 0, buf, -1, NULL, 0, NULL, NULL);
    std::vector<char> utf8(len);
	WideCharToMultiByte(CP_ACP, 0, buf, -1, &utf8[0], len, NULL, NULL);

    return std::string(&utf8[0]);
}

std::string MB2UT(const char* buf) {
    int len = MultiByteToWideChar(CP_ACP, 0, buf, -1, NULL, 0);
    std::vector<wchar_t> unicode(len);
    MultiByteToWideChar(CP_ACP, 0, buf, -1, &unicode[0], len);
    len = WideCharToMultiByte(CP_UTF8, 0, &unicode[0], -1, NULL, 0, NULL, NULL);
    std::vector<char> utf8(len);
    WideCharToMultiByte(CP_UTF8, 0, &unicode[0], -1, &utf8[0], len, NULL, NULL);
    return std::string(&utf8[0]);
}

std::string UT2MB(const char* buf) {
    int len = MultiByteToWideChar(CP_UTF8, 0, buf, -1, NULL, 0);
    std::vector<wchar_t> unicode(len);
    MultiByteToWideChar(CP_UTF8, 0, buf, -1, &unicode[0], len);
    len = WideCharToMultiByte(CP_ACP, 0, &unicode[0], -1, NULL, 0, NULL, NULL);
    std::vector<char> gb2312(len);
    WideCharToMultiByte(CP_ACP, 0, &unicode[0], -1, &gb2312[0], len, NULL, NULL);
    return std::string(&gb2312[0]);
}

#endif
