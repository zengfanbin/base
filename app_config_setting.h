#ifndef APP_CONFIG_SETTING_H
#define APP_CONFIG_SETTING_H

#include <QSettings>
#include <QApplication>

class AppConfigSetting
{
private:
    QSettings *settings;

public:
    static AppConfigSetting &instance()
    {
        static AppConfigSetting instance;
        return instance;
    }

    void set(const QString &key, const QVariant &value);

    QVariant get(const QString &key, const QVariant &defaultValue= QVariant()) const ;

    void remove(const QString &key);

    bool contains(const QString &key);

    AppConfigSetting(AppConfigSetting const &) = delete;
    AppConfigSetting(AppConfigSetting &&) = delete;
    AppConfigSetting &operator=(AppConfigSetting const &) = delete;
    AppConfigSetting &operator=(AppConfigSetting &&) = delete;

    virtual ~AppConfigSetting();

protected:
    explicit AppConfigSetting();
};

#endif // APP_CONFIG_SETTING_H
