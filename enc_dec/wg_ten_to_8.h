#ifndef WG_TEN_TO_8_H
#define WG_TEN_TO_8_H

#include <QString>

QString wgTenTo8(const QString &ten);

#endif // WG_TEN_TO_8_H
