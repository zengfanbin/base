#-------------------------------------------------
#
# Project created by QtCreator 2017-02-20T21:59:47
#
#-------------------------------------------------

QT       += core network gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

QMAKE_CXXFLAGS += -std=c++11

TARGET = qtbase
CONFIG   += console
CONFIG   -= app_bundle

win32: {
    LIBS += -lWlanapi
}

TEMPLATE = app

SOURCES += \
    main.cpp \
    base.cpp \
    container/HashList.cpp \
    container/RingQueue.cpp \
    enc_dec/wg_ten_to_8.cpp \
    log.cpp \
    enc_dec/base64.cpp \
    enc_dec/md5.c \
    app_config_setting.cpp \
    net/httpdownload.cpp \
    string/charset.cpp \
    system/systeminfo.cpp \
    ui_component/filedialog.cpp

HEADERS += \
    base.h \
    container/HashList.h \
    container/RingQueue.h \
    enc_dec/wg_ten_to_8.h \
    log.h \
    enc_dec/base64.h \
    enc_dec/md5.h \
    app_config_setting.h \
    net/httpdownload.h \
    string/string_ext.h \
    string/charset.h \
    system/systeminfo.h \
    ui_component/filedialog.h

DISTFILES += \
    README.md
